<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<div class="container">
        <div class="row justify-content-center">
        <div class="border border-primary rounded mt-3">
            <div class="container p-3 my-3 bg-primary text-white">
    <!-- Form Input Tabel Karyawan -->
    <center>
            <h1>Formulir Input Data Karyawan</h1>
                <p>Masukkan data karyawan pada formulir di bawah </p>
            </center>
            </div>
            @foreach ($karyawan as $row )
            <form action="/crudweb/update" method="post" enctype="multipart/form-data">                      
            @csrf
            <input type="hidden" name="id" value="{{ $row->id }}">
    <label for="Nama Karyawan">Nama Karyawan</label><br>
    <input type="text" class= form-control name="nama" value="{{ $row->nama_karyawan }}" required><br>
    <label for="NIP">No Karyawan</label><br>
    <input type="text" class= form-control name="no"   value="{{ $row->no_karyawan }}"required><br>

    <label for="text">Telepon</label><br>
    <input type="text" class= form-control name="telp" value="{{ $row->no_telp_karyawan }}" required><br>
    <label for="text">Jabatan</label><br>
    <input type="text" class= form-control name="jabatan"value="{{ $row->jabatan_karyawan }}" required><br>
    <label for="text">Divisi</label><br>
    <input type="text" class= form-control name="divisi" value="{{ $row->divisi_karyawan }}"required><br>

   
    <center>
    <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
    <center>
    
    </form>
    @endforeach
</div>
</body>
</html>



