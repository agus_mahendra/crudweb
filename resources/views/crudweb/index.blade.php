<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Karyawan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<div class="container">

<h1 class="text-center">Tabel Karyawan</h1>
<a href="/crudweb/create" class="btn btn-primary mb-4">Create</a>

    <table class="table border">
         @foreach ($karyawan as $row )
        <thead>
            <tr>
            <th>ID</th>
            <th>Nama Karyawan</th>
            <th>No Karyawan</th>
            <th>No Telepon</th>
            <th>Jabatan</th>
            <th>Divisi</th>
            <th>Aksi</th>
            </tr>
        </thead>
       <tbody> 
           <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->nama_karyawan}}</td>
                <td>{{$row->no_karyawan}}</td>
                <td>{{$row->no_telp_karyawan}}</td>
                <td>{{ $row->jabatan_karyawan }}</td>
                <td>{{ $row->divisi_karyawan }}</td>
                <td>
                    <a href="/crudweb/edit/{{$row->id}}" class="btn btn-primary ">Edit</a>
                    <a href="/crudweb/hapus/{{ $row->id }}" class="btn btn-primary ">Delete</a>
                </td>
            </tr>
         
        </tbody>
        @endforeach
    </table>
    </div>
</body>
</html>



