<?php

use App\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CrudController::class, 'index']);
Route::get('/home', [CrudController::class, 'index']);
//route untuk create data 
Route::get('/crudweb/create', [CrudController::class, 'create']);
//route untuk simpan data
Route::post('/crudweb/simpan', [CrudController::class, 'simpan']);
//route untuk edit data
Route::get('/crudweb/edit/{id}', [CrudController::class, 'edit']);
//route untuk mengupdate data yang sudah diedit
Route::post('/crudweb/update', [CrudController::class, 'update']);
//route untuk menghapus data
Route::get('/crudweb/hapus/{id}', [CrudController::class, 'destroy']);